package main

import (
	"flag"
	"fmt"
	"net/http"
	"time"

	spoe "github.com/criteo/haproxy-spoe-go"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	configFile   = flag.String("config", "config.yml", "Path to YAML formatted config")
	debug        = flag.Bool("debug", false, "Enable debug logging")
	readtimeout  = flag.Duration("read-timeout", 5*time.Second, "HTTP read timeout")
	writetimeout = flag.Duration("write-timeout", 5*time.Second, "HTTP write timeout")
	idletimeout  = flag.Duration("idle-timeout", 60*time.Second, "HTTP idle timeout")
)

func main() {

	// Define vars
	var err error
	var srv *http.Server

	flag.Parse()

	// Enable debug
	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	// Create loggers for later
	spoaLogger := log.WithField("source", "spoa")
	configLogger := log.WithField("source", "config")

	// Set defaults and initialise viper
	initViper(*configFile)
	if err := viper.ReadInConfig(); err != nil {
		configLogger.WithField("error", err).Fatalf("Problem loading config")
	}

	if err := validateConfig(); err != nil {
		configLogger.WithField("error", err).Fatalf("invalid config")
	}

	// Load service provider list
	splist, err := newServiceProviderList()
	if err != nil {
		configLogger.WithField("error", err).Fatalf("Problem building service provider list")
	}

	// create new server
	srv = &http.Server{
		Handler:      newRouter(splist),
		Addr:         fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("port")),
		WriteTimeout: *readtimeout,
		ReadTimeout:  *writetimeout,
		IdleTimeout:  *idletimeout,
	}

	switch viper.GetString("tls.method") {
	case "cfssl":
		cfsslServe(srv)
	case "user":
		userServe(srv)
	default:
		configLogger.Fatal("tls.method is invalid")
	}

	// start spoe server
	agent := spoe.New(splist.agentHandler)
	if err := agent.ListenAndServe(fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("spoe_port"))); err != nil {
		spoaLogger.WithField("error", err).Fatal("agent error")
	}
}
