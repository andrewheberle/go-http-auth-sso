package main

import (
	"fmt"
	"net/http"
	"net/url"
	"path/filepath"
	"testing"

	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func TestConfig_catchAllHandler(t *testing.T) {
	assert.HTTPError(t, catchAllHandler, "GET", "/", nil)
}

func TestConfig_healthHandler(t *testing.T) {
	assert.HTTPSuccess(t, healthHandler, "GET", "/", nil)
}

func Test_loginHandler(t *testing.T) {
	tests := []struct {
		method    string
		url       string
		values    url.Values
		wanterror bool
	}{
		{"GET", "/auth/login", nil, false},
		{"GET", "/auth/login", url.Values{"returnurl": []string{"https://www.example.net"}}, false},
		{"GET", "/auth/login", url.Values{"returnurl": []string{"https://www.ex ample.net"}}, true},
	}

	for _, tt := range tests {
		if tt.wanterror {
			assert.HTTPError(t, loginHandler, tt.method, tt.url, tt.values)
		} else {
			assert.HTTPRedirect(t, loginHandler, tt.method, tt.url, tt.values)
		}
	}
}

func TestConfig_agentHandler(t *testing.T) {

	tests := []struct {
		name     string
		messages []spoe.Message
		actions  []spoe.Action
		wanterr  bool
	}{
		{"empty request", []spoe.Message{}, []spoe.Action{}, false},
		{"unsupported message", []spoe.Message{
			{
				Name: "invalid",
				Args: map[string]interface{}{"blah": "blah"},
			},
		}, []spoe.Action{}, false},
		{"supported message, missing args", []spoe.Message{
			{
				Name: "auth-request",
				Args: map[string]interface{}{"blah": "blah"},
			},
		}, nil, true},
		{"supported message, invalid provider arg", []spoe.Message{
			{
				Name: "auth-request",
				Args: map[string]interface{}{"provider": "invalidprovider"},
			},
		}, nil, true},
		{"supported message, invalid provider arg with string header", []spoe.Message{
			{
				Name: "auth-request",
				Args: map[string]interface{}{
					"provider": "invalidprovider",
					"headers":  fmt.Sprintf("Some-Header: value\n\n"),
				},
			},
		}, nil, true},
		{"supported message, valid provider with string header",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider": "samltest",
						"headers":  fmt.Sprintf("Some-Header: value\n\n"),
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			false,
		},
		{"supported message, valid provider with invalid string header",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider": "samltest",
						"headers":  fmt.Sprintf("Some-Header: value"),
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			true,
		},
		{"supported message, valid provider with invalid binary header",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider": "samltest",
						"headers": []byte{
							11, 83, 111, 109, 101, 45, 72, 101, 97, 100, 101, 114,
							4, 98, 108, 97, 104,
						},
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			true,
		},
		{"supported message, valid provider with binary header with invalid token",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider": "samltest",
						"headers": []byte{
							11, 83, 111, 109, 101, 45, 72, 101, 97, 100, 101, 114,
							4, 98, 108, 97, 104,
							6, 67, 111, 111, 107, 105, 101,
							20, 116, 111, 107, 101, 110, 61, 97, 110, 105, 110, 118, 97, 108, 105, 100, 116, 111, 107, 101, 110,
							0, 0,
						},
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			false,
		},
		{"supported message, valid provider arg with string header with invalid token",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider": "samltest",
						"headers":  fmt.Sprintf("Some-Header: value\nCookie: token=aninvalidtoken\n\n"),
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			false,
		},
		{"supported message, valid provider arg with string header with invalid token and invalid constraints",
			[]spoe.Message{
				{
					Name: "auth-request",
					Args: map[string]interface{}{
						"provider":    "samltest",
						"constraints": "invalid",
						"headers":     fmt.Sprintf("Some-Header: value\nCookie: token=aninvalidtoken\n\n"),
					},
				},
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "response_code",
					Scope: spoe.VarScopeTransaction,
					Value: http.StatusUnauthorized,
				},
				spoe.ActionSetVar{
					Name:  "auth_successful",
					Scope: spoe.VarScopeTransaction,
					Value: false,
				},
			},
			true,
		},
	}

	configs := []struct {
		name   string
		config string
	}{
		{"tls.method == user", "config-user.yml"},
		{"tls.method == cfssl", "config-cfssl.yml"},
	}

	for _, cc := range configs {
		// Load in test config
		viper.Reset()
		initViper(filepath.Join("testdata", cc.config))
		if !assert.Nil(t, viper.ReadInConfig()) {
			return
		}
		if !assert.Nil(t, validateConfig()) {
			return
		}

		splist, err := newServiceProviderList()
		if !assert.Nil(t, err, "newServiceProviderList") {
			return
		}

		for _, tt := range tests {
			actions, err := splist.agentHandler(tt.messages)
			if tt.wanterr {
				assert.NotNil(t, err, tt.name)
			} else {
				if assert.Nil(t, err, tt.name) {
					assert.Equal(t, tt.actions, actions, tt.name)
				}
			}
		}
	}
}

func Test_getMsgArgsHeaders(t *testing.T) {
	tests := []struct {
		name    string
		msg     spoe.Message
		want    http.Header
		wanterr bool
	}{
		{"no headers provided", spoe.Message{}, nil, true},
		{"invalid type headers provided", spoe.Message{
			Name: "auth-request",
			Args: map[string]interface{}{"headers": 100},
		}, nil, true},
		{"invalid string headers provided", spoe.Message{
			Name: "auth-request",
			Args: map[string]interface{}{"headers": fmt.Sprintf("X-Foo: value")},
		}, nil, true},
		{"valid string headers provided", spoe.Message{
			Name: "auth-request",
			Args: map[string]interface{}{"headers": fmt.Sprintf("X-Foo: value\n\n")},
		}, http.Header{"X-Foo": []string{"value"}}, false},
	}

	for _, tt := range tests {
		got, err := getMsgArgsHeaders(tt.msg)
		if tt.wanterr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
			assert.Equal(t, tt.want, got, tt.name)
		}
	}
}
