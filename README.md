# go-http-auth-sso

[![pipeline status](https://gitlab.com/andrewheberle/go-http-auth-sso/badges/master/pipeline.svg)](https://gitlab.com/andrewheberle/go-http-auth-sso/commits/master)
[![coverage report](https://gitlab.com/andrewheberle/go-http-auth-sso/badges/master/coverage.svg)](https://gitlab.com/andrewheberle/go-http-auth-sso/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/andrewheberle/go-http-auth-sso)](https://goreportcard.com/report/gitlab.com/andrewheberle/go-http-auth-sso)

This project started out as as a way to implement a single sign on (SSO)
solution for [HAProxy](https://www.haproxy.org/).

Based on the current design, this code implements a `Stream Processing Offload
Engine` (SPOE) that is used to check if a user is logged in and a `SAML Service
Provider` (SP) to perform initiate the sign on process and allow login status
to be retrieved internally.

* [Installation](#installation)
* [Usage](#usage)
* [Configuration](#configuration)
* [Example](#example)
* [Limitations and Known Issues](#limitations-and-known-issues)
* [License](#license)

## Installation

```sh
go get -u gitlab.com/andrewheberle/go-http-auth-sso
```

Please note that builds are only tested on Go 1.12 and 1.13. Older releases of Go may work fine, but are untested.

## Usage

```sh
go-http-auth-sso -config /path/to/config.yml
```

A configuration file must be used to define (at minimum) an authentication
provider to use. In addition a spoe configuration file must be created for
HAProxy with the matching "filter" definition in the main HAProxy config.

This `Stream Processing Offload Agent` (SPOA) requires the request headers to
be provided as arguments in the SPOE configuration. For more information on
SPOE and the configuration see:

* [HAProxy SPOE Documentation](https://www.haproxy.org/download/2.0/doc/SPOE.txt)
* [Extending HAProxy with the Stream Processing Offload Engine](https://www.haproxy.com/blog/extending-haproxy-with-the-stream-processing-offload-engine/)

### Configuration

The main configuration file can be specified on the command line (it defaults
to "config.yml")

The following configuration options can be set:

* [basepath](#basepath)
* [address](#address)
* [port](#port)
* [spoe_port](#spoe_port)
* [healthcheck_url](#healthcheck_url)
* [log_healthchecks](#log_healthchecks)
* [default_auth](#default_auth)
* [tls](#tls)
  * method
  * key
  * certificate
  * [cfssl](#cfssl)
    * cn
    * hosts
    * profile
    * remote
    * auth_type
    * auth_key
* [auth](#auth)
  * name
    * root
    * login_url
    * certificate
    * key
    * metadata
    * cookie_name
    * cookie_domain
    * claim_mapping
* [constraints](#constraints)
  * name
    * claim

#### basepath

Defines the base path for any non-absolute paths referenced in the
configuration, such as ssl keys and certificates.

Default: . (ie current directory)

#### address

Listen address for SAML SP and SPOE.

Default: 127.0.0.1

#### port

Listen port for SAML SP.

Default: 8443

#### spoe_port

Listen port for SPOE.

Default: 9000

#### healthcheck_url

URL to respond to health checks on.

Default: /healthz

#### log_healthchecks

Enables or disables logging of access to the "healthcheck_url".

Default: false

#### default_auth

The default auth provider if its not specified in request via SPOA from HAProxy.

#### tls

##### method

Can be either `user` or `cfssl`:

* user: use provided certificate and keys as-is (default)
* cfssl: use a remote cfssl instance to request certificates automatically

##### key

Location of the BASE64/PEM format private key. This key is used as part of the
keypair to secure https access to the SAML Service Provider.

Default: server.key

##### certificate

Location of the BASE64/PEM format ssl certificate. This certificate is used as
part of the keypair to secure https access to the SAML Service Provider.

Default: server.pem

##### cfssl

This section is used to configure the automatic certificate request process if
cfssl was set in the "tls.method" option.

See [CFSSL](https://github.com/cloudflare/cfssl) for further info on CFSSL in
general.

###### cn

The Common Name for the certificate request.

Default: localhost

###### remote

The CFSSL instance to request a certificate from.

Default: localhost:8000

###### profile

The certificate profile to request.

Default: server

###### hosts

Subject Alternative Names (SAN's) to add to certificate request.

Default: no default

Notes: "cn" will always be added to "hosts" as a SAN as per RFC2818. In
addition, any names associated with configured SAML SP's will be added too.

###### auth_type

Authentication method to use against the configured CFSSL remote.

Default: standard

###### auth_key

Authentication key to use against the configured CFSSL remote.

Default: no default

##### auth

This is a list of SAML Service Providers to intitialise in order to
authenticate users against.

###### root

This is the root url of the SAML Service Provider.

Default: no default

Example: <https://sso.example.net>

###### login_url

The url to initiate the SAML login process.

Default: /auth/login

###### certificate

This is the certificate used by the SAML SP for token signing/encryption.

This is usually self signed.

```sh
openssl req -x509 -newkey rsa:2048 -keyout server.key -out server.pem -days 365 -nodes -subj "/CN=sso.example.net"
```

###### key

This is the key used by the SAML SP for token signing/encryption.

This is usually part of a self signed keypair.

```sh
openssl req -x509 -newkey rsa:2048 -keyout server.key -out server.pem -days 365 -nodes -subj "/CN=sso.example.net"
```

###### metadata

This is the URL to retrieve the metadata from for the SAML Identity Provider (IdP).

Example: <https://samltest.id/saml/idp>

## Example

The following shows an example configuration that requires authentication for
all access to a frontend and then sets a custom header that the backend app
can use for authentication/authorisation purposes.

This example uses the new "program" section in HAProxy 2.0 to start the
"go-http-auth-sso" service, however this could just as easily be handled by
your favourite init system or daemon supervision suite.

### Files

* /etc/haproxy/spoe.cfg: SPOE filter config file
* /etc/haproxy/haproxy.cfg: HAProxy config file
* /etc/haproxy/ssl/server.pem: HAProxy SSL certificate/key
* /etc/haproxy/ca-bundle.pem: CA bundle to allow HAProxy to verify server certs
* /opt/go-http-auth-sso/etc/config.yml: Our config file
* /opt/go-http-auth-sso/etc/ssl/server.key: Our HTTPS private key
* /opt/go-http-auth-sso/etc/ssl/server.pem: Our HTTPS certificate
* /opt/go-http-auth-sso/etc/ssl/sp/server.key: Our Service Provider key
* /opt/go-http-auth-sso/etc/ssl/sp/server.pem: Our Service Provider cert

### /etc/haproxy/spoe.cfg

```haproxy
[engine-name]

spoe-agent agent-name
    log global
    option var-prefix auth_request

    timeout hello      2s
    timeout processing 10ms
    timeout idle       2m

    groups group-name

    use-backend spoe-backend-name

spoe-message auth-request
    args provider=str("myssoservice")
    args headers=req.hdrs

spoe-group group-name
    messages auth-request

```

In the above configuration, the provider name and request headers are passed
to the SPOA.

The "headers" argument is required while "provider" is optional assuming a
"default_auth" option is set in the configuration file.

In addition a "constraints" argument can be provided which enforces the
configured constraints.

The "headers" argument may be provided via the "req.hdrs" or the "req.hdrs_bin"
HAProxy sample fetch.

### /etc/haproxy/haproxy.cfg

```haproxy
global
    user haproxy
    group haproxy

    master-worker
    ...

program spoe-auth-program
    # execute go-http-auth-sso along with HAProxy
    command /opt/go-http-auth-sso/bin/go-http-auth-sso -config /opt/go-http-auth-sso/etc/config.yml

defaults
    # define some defaults
    mode                    http
    log                     global
    option                  httplog
    option                  forwardfor
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    timeout tunnel          1h

frontend app-frontend
    # Listen on HTTP and HTTPS
    bind :80
    bind :443 ssl crt /etc/haproxy/ssl/server.pem

    # Enable spoe filter
    filter spoe engine engine-name config /etc/haproxy/spoe.cfg

    # ACLs for SAML SP
    acl sso_host hdr(host) -i sso.example.net
    acl sso_url  path_beg -i /saml
    acl sso_url  path -i /auth/login

    # Auth ACLs
    acl auth_required   var(txn.auth_request.response_code) 401
    acl auth_successful var(txn.auth_request.auth_successful) -m bool

    # Redirect to HTTPS
    http-request redirect scheme https if !{ ssl_fc }

    # Do auth for all requests excep to the SP itself
    http-request send-spoe-group engine-name group-name unless sso_host sso_url

    # based on response either force login or deny access
    http-request redirect code 302 location https://sso.example.net/auth/login?returnurl=https://%[hdr(host)]%[capture.req.uri] if auth_required
    http-request deny if ! auth_successful

    use_backend sso-backend-name if sso_host sso_url

    default_backend app-backend-name

backend sso-backend-name
    option httpchk GET /healthz
    server auth-server-https 127.0.0.1:8443 check ssl ca-file /etc/haproxy/ca-bundle.pem

backend spoe-backend-name
    mode tcp
    option tcplog
    timeout connect 5s
    timeout server  3m
    server auth-server-spoe 127.0.0.1:9000

backend app-backend-name
    http-request del-header X-Auth-Username if !{ var(txn.auth_request.uid) -m found }
    http-request set-header X-Auth-Username %[var(txn.auth_request.uid)] if { var(txn.auth_request.uid) -m found }

    server app <app ip>:<app port> check ssl ca-file /etc/haproxy/ca-bundle.pem
```

The important parts of the above config is enabling of the SPOE "filter", the
"http-request send-spoe-group" line to send the request to the configured
"filter" and the "http-request redirect/deny" lines to act on the result.

The "http-request del-header/set-header" lines in the "app" backend allow
the backend web application to consult these headers for authentication and
authorisation purposes.

As the header is removed if the expected variable is not present and set only
if is, there is no way for the user to "spoof" this header themselves as long
as the backend web application is ONLY accessible via HAProxy. A user with
direct access to the backend could trivially add the "X-Auth-Username" header
in order to impersonate a logged in user.

### /opt/go-http-auth-sso/etc/config.yml

```yaml
---
basepath: /opt/go-http-auth-sso/etc
tls:
  method: user
  key: ssl/server.key
  certificate: ssl/server.pem
auth:
  myssoservice:
    root: https://sso.example.net
    login_url: /auth/login
    certificate: ssl/sp/server.pem
    key: ssl/sp/server.key
    metadata: https://samltest.id/saml/idp
    cookie_domain: .example.net
```

The above config assumes SSL keys and certificates are located in:

* /opt/go-http-auth-sso/etc/ssl

## Limitations and Known Issues

* There is currently no "Log Out" option or feature.
* Potentially terrible code :)

## License

This project is licensed under the terms of the MIT license.
