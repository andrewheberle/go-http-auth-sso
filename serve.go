package main

import (
	"fmt"
	"net"
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func userServe(srv *http.Server) {
	// plain old Listen with ServerTLS required
	logger := log.WithField("source", "usertls")
	nl, err := net.Listen("tcp", fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("port")))
	if err != nil {
		logger.WithField("error", err).Fatal("Problem creating listener")
	}
	go func() {
		if err := srv.ServeTLS(nl, viper.GetString("tls.certificate"), viper.GetString("tls.key")); err != nil {
			logger.WithField("error", err).Fatal("http server error")
		}
	}()
}

func cfsslServe(srv *http.Server) {
	// create cfssl transport
	logger := log.WithField("source", "cfssl")
	tl, err := startCFSSLListener()
	if err != nil {
		logger.WithField("error", err).Fatal("problem creating transport")
	}
	var errChan = make(chan error)
	go func(ec <-chan error) {
		for {
			err, ok := <-ec
			if !ok {
				logger.Warn("error channel closed, future errors will not be reported")
				break
			}
			logger.WithField("error", err).Warn("auto update error")
		}
	}(errChan)

	logger.Info("setting up auto-update")
	go tl.AutoUpdate(nil, errChan)
	go func() {
		if err := srv.Serve(tl); err != nil {
			logger.WithField("error", err).Fatal("http server error")
		}
	}()
}
