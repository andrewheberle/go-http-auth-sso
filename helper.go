package main

import (
	"bufio"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/crewjam/saml/samlsp"
	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/spf13/viper"
	errors "golang.org/x/xerrors"
)

// contains returns true if a contains b
func contains(a []string, b string) bool {
	for _, n := range a {
		if b == n {
			return true
		}
	}
	return false
}

func parseStringHeader(s string) (h http.Header, err error) {
	var blankFound bool
	h = make(http.Header)
	scanner := bufio.NewScanner(strings.NewReader(s))
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			blankFound = true
			break
		}
		hdr := strings.SplitN(line, ": ", 2)
		if len(hdr) == 2 {
			h.Add(http.CanonicalHeaderKey(hdr[0]), hdr[1])
		}
	}
	if !blankFound {
		return nil, errors.Errorf("headers possibly trucated as final blank line not found")
	}
	return h, nil
}

func parseBinaryHeader(b []byte) (h http.Header, err error) {
	var blankFound bool
	h = make(http.Header)

	p := 0
	for p < len(b) {
		// start from p in slice
		buf := b[p:]

		// decode header name
		name, n, err := decodeString(buf)
		if err != nil {
			return nil, errors.Errorf("parse binary header name: %w", err)
		}

		// advance n bytes if possible
		if p+n >= len(b) {
			return nil, errors.Errorf("parse binary header name: went past end of slice")
		}
		p = p + n

		// start from new p
		buf = b[p:]
		value, n, err := decodeString(buf)
		if err != nil {
			return nil, errors.Errorf("parse binary header value: %w", err)
		}

		// advance n bytes if possible
		if p+n > len(b) {
			return nil, errors.Errorf("parse binary header value: went past end of slice")
		}
		p = p + n

		// final is blank header and value
		if name == "" && value == "" {
			blankFound = true
			break
		}

		// add decoded data
		h.Add(http.CanonicalHeaderKey(name), value)
	}
	if !blankFound {
		return nil, errors.Errorf("headers possibly trucated as final blank line not found")
	}
	return h, nil
}

func makeSubKeyAbsPath(sub *viper.Viper, key string) error {
	if !sub.IsSet(key) {
		return errors.Errorf("key not found: %w", key)
	}

	p := sub.GetString(key)

	if filepath.IsAbs(p) {
		return nil
	}

	sub.Set(key, filepath.Join(viper.GetString("basepath"), p))

	return nil
}

func makeKeyAbsPath(key string) error {
	return makeSubKeyAbsPath(viper.GetViper(), key)
}

func makeSubKeyAbsPathAndGet(sub *viper.Viper, key string) (string, error) {
	if err := makeSubKeyAbsPath(sub, key); err != nil {
		return "", err
	}

	return sub.GetString(key), nil
}

func makeKeyAbsPathAndGet(key string) (string, error) {
	if err := makeSubKeyAbsPath(viper.GetViper(), key); err != nil {
		return "", err
	}

	return viper.GetString(key), nil
}

func parseClaims(claims map[string]string) (actions []spoe.Action) {
	actions = make([]spoe.Action, 0)
	for k, v := range claims {
		actions = append(actions, spoe.ActionSetVar{
			Name:  k,
			Scope: spoe.VarScopeTransaction,
			Value: v,
		})
	}
	return actions
}

func constraintsValid(constraints map[string][]string, claims map[string]string) bool {
	for k, v := range constraints {
		// Get attribute specified in constraints
		attr, ok := claims[k]
		// Check if constraint key was found in claims
		if !ok {
			return false
		}

		// Constraint key found, check if it's valid
		if !contains(v, attr) {
			return false
		}
	}

	return true
}

func parseToken(attributes samlsp.Attributes, mapping map[string]string) (claims map[string]string) {
	claims = make(map[string]string)

	// Do mapping if requested
	if mapping != nil {
		for k, v := range mapping {
			if attr := attributes.Get(v); attr != "" {
				claims[k] = attr
			}
		}

		return claims
	}

	// Otherwise straight attribute -> claim
	for k := range attributes {
		claims[k] = attributes.Get(k)
	}

	return claims
}
