package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/crewjam/saml/samlsp"
	log "github.com/sirupsen/logrus"

	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	errors "golang.org/x/xerrors"
)

func catchAllHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	// This should do some sort of service level checks
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "{ \"status\": \"success\", \"message\": \"Service health is OK\" }")
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	returnurl := r.URL.Query().Get("returnurl")

	// Redirect to root if no return url was provided
	if returnurl == "" {
		http.Redirect(w, r, "//"+r.Host+"/", http.StatusFound)
		return
	}

	// Ensure URL is valid
	returnURL, err := url.Parse(returnurl)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Redirect to URL
	http.Redirect(w, r, returnURL.String(), http.StatusFound)
}

// agentHandler function
func (s serviceProviders) agentHandler(messages []spoe.Message) ([]spoe.Action, error) {
	var statusCode int
	var constraints map[string][]string
	var parsedHeaders http.Header
	var err error

	claims := make(map[string]string)
	actions := make([]spoe.Action, 0)

	// Default fields
	agentLogger := log.WithFields(log.Fields{"source": "agent"})

	agentLogger.WithFields(log.Fields{"action": "messages"}).Debug("handling messages")

	for _, msg := range messages {
		if msg.Name != "auth-request" {
			continue
		}

		provider, ok := msg.Args["provider"].(string)
		if !ok {
			if !viper.IsSet("default_auth_provider") {
				return nil, errors.Errorf("spoe handler: expected provider in message and no default set")
			}
			provider = viper.GetString("default_auth_provider")
			agentLogger.WithField("provider", provider).Debug("using default provider")
		}

		// Check for an retrieve "headers" argument in message
		if parsedHeaders, err = getMsgArgsHeaders(msg); err != nil {
			return nil, errors.Errorf("spoe handler: %w", err)
		}

		cs, ok := msg.Args["constraints"].(string)
		if ok {
			if !viper.IsSet("constraints." + cs) {
				return nil, errors.Errorf("spoe handler: constraints requested but not found")
			}
			constraints = viper.GetStringMapStringSlice("constraints." + cs)
		}

		// check provider is found
		p, ok := s[provider]
		if !ok {
			return nil, errors.Errorf("spoe handler: provider not found")
		}

		// create dummy router
		router := mux.NewRouter()
		router.Host(p.Host).Path("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			session := samlsp.SessionFromContext(r.Context())
			// nil session here == not logged in
			if session == nil {
				statusCode = http.StatusUnauthorized
				w.WriteHeader(statusCode)
				return
			}

			// cast as SessionWithAttributes
			sessionWithAttributes, ok := session.(samlsp.SessionWithAttributes)
			if !ok {
				statusCode = http.StatusInternalServerError
				w.WriteHeader(statusCode)
				return
			}

			// parse attributes into claims
			claims = parseToken(sessionWithAttributes.GetAttributes(), p.ClaimMapping)

			// Check for any constraints
			if !constraintsValid(constraints, claims) {
				statusCode = http.StatusForbidden
				w.WriteHeader(statusCode)
				return
			}

			// return final status
			statusCode = http.StatusOK
			w.WriteHeader(statusCode)
		})

		// build request
		req, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			return nil, errors.Errorf("spoe handler: %w", err)
		}

		// fudge headers
		req.Host = s[provider].Host
		req.Header = parsedHeaders
		req.Header.Set("Host", req.Host)

		// create response recorder
		rr := httptest.NewRecorder()

		// run handler
		router.ServeHTTP(rr, req)

		setVarLogger := agentLogger.WithFields(log.Fields{
			"action": "set-var",
			"scope":  "txn",
		})
		// add response code
		setVarLogger.WithFields(log.Fields{
			"name":  "response_code",
			"value": statusCode,
		}).Debug("spoe action")
		actions = append(actions, spoe.ActionSetVar{
			Name:  "response_code",
			Scope: spoe.VarScopeTransaction,
			Value: statusCode,
		})

		// add auth status code
		setVarLogger.WithFields(log.Fields{
			"name":  "auth_successful",
			"value": statusCode == 200,
		}).Debug("spoe action")
		actions = append(actions, spoe.ActionSetVar{
			Name:  "auth_successful",
			Scope: spoe.VarScopeTransaction,
			Value: statusCode == 200,
		})

		// add claims
		actions = append(actions, parseClaims(claims)...)
	}

	return actions, nil
}

func getMsgArgsHeaders(msg spoe.Message) (parsedHeaders http.Header, err error) {
	// check for headers argument
	if _, ok := msg.Args["headers"]; !ok {
		return nil, errors.Errorf("missing headers argument in message")
	}

	// handle headers as string or binary types
	switch headers := msg.Args["headers"].(type) {
	case string:
		parsedHeaders, err = parseStringHeader(headers)
	case []byte:
		parsedHeaders, err = parseBinaryHeader(headers)
	default:
		return nil, errors.Errorf("invalid type for headers")
	}
	if err != nil {
		return nil, errors.Errorf("error parsing header argument: %w", err)
	}

	return
}
