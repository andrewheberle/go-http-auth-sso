package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		middlewareLogger := log.WithFields(log.Fields{
			"source":      "http",
			"remote-addr": r.RemoteAddr,
			"host":        r.Host,
			"method":      r.Method,
			"request-uri": r.RequestURI,
			"proto":       r.Proto,
		})
		if r.RequestURI == viper.GetString("healthcheck_url") {
			// Only log access to health check URI if LogHealthChecks == true
			if viper.GetBool("log_healthchecks") {
				middlewareLogger.Info("http request")
			}
		} else {
			// Log everything else
			middlewareLogger.Info("http request")
		}

		// Call next handler/middleware
		next.ServeHTTP(w, r)
	})
}
