package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func newRouter(spList serviceProviders) *mux.Router {
	r := mux.NewRouter()
	r.Use(loggingMiddleware)

	// Add routes for any service providers
	for _, p := range spList {
		s := r.Host(p.Host).Subrouter()
		s.PathPrefix("/saml/").Handler(p.Provider)
		s.Path(p.LoginURL).Handler(p.Provider.RequireAccount(http.HandlerFunc(loginHandler)))
	}

	// Health check and catch all handler
	r.HandleFunc(viper.GetString("healthcheck_url"), healthHandler)
	r.PathPrefix("/").HandlerFunc(catchAllHandler)

	return r
}
