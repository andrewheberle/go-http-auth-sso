package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_decodeVarint(t *testing.T) {
	tests := []struct {
		name    string
		b       []byte
		wantval int
		wantoff int
		wantErr bool
	}{
		{"empty slice", []byte{}, 0, 0, true},
		{"value = 0", []byte{0}, 0, 1, false},
		{"value = 128", []byte{128}, 128, 1, false},
		{"unterminate sequence", []byte{255}, 0, 0, true},
		{"value = 271", []byte{255, 1}, 271, 2, false},
	}
	for _, tt := range tests {
		gotval, gotoff, err := decodeVarint(tt.b)
		if tt.wantErr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
			assert.Equal(t, tt.wantval, gotval, tt.name)
			assert.Equal(t, tt.wantoff, gotoff, tt.name)
		}

	}
}

func Test_decodeBytes(t *testing.T) {
	tests := []struct {
		name    string
		b       []byte
		wantb   []byte
		wantoff int
		wantErr bool
	}{
		{"empty slice", []byte{}, []byte{}, 0, true},
		{"invalid", []byte{5}, []byte{}, 0, true},
		{"single 1 byte value", []byte{1, 1}, []byte{1}, 2, false},
		{"two 1 byte values", []byte{1, 1, 1, 1}, []byte{1}, 2, false},
		{"short value", []byte{4, 1, 1, 1}, []byte{}, 0, true},
		{"four byte value", []byte{4, 1, 1, 1, 1}, []byte{1, 1, 1, 1}, 5, false},
	}
	for _, tt := range tests {
		gotb, gotoff, err := decodeBytes(tt.b)
		if tt.wantErr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
			assert.Equal(t, tt.wantb, gotb, tt.name)
			assert.Equal(t, tt.wantoff, gotoff, tt.name)
		}

	}
}

func Test_decodeString(t *testing.T) {
	tests := []struct {
		name    string
		b       []byte
		want    string
		wantoff int
		wantErr bool
	}{
		{"empty slice", []byte{}, "", 0, true},
		{"invalid", []byte{5}, "", 0, true},
		{"single 1 byte value", []byte{1, 97}, "a", 2, false},
		{"two 1 byte values", []byte{1, 97, 1, 97}, "a", 2, false},
		{"short value", []byte{4, 1, 1, 1}, "", 0, true},
		{"four byte value", []byte{4, 97, 98, 99, 100}, "abcd", 5, false},
	}
	for _, tt := range tests {
		got, gotoff, err := decodeString(tt.b)
		if tt.wantErr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
			assert.Equal(t, tt.want, got, tt.name)
			assert.Equal(t, tt.wantoff, gotoff, tt.name)
		}

	}
}
