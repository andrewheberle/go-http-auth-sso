package main

import (
	"net"
	"time"

	errors "golang.org/x/xerrors"
)

// dummy types used in windows noop wrappers
type transportListener struct {
	net.Listener
}

// AutoUpdate windows wrapper for (transport.Listener).AutoUpdate
func (l *transportListener) AutoUpdate(certUpdates chan<- time.Time, errChan chan<- error) {

}

func startCFSSLListener() (l *transportListener, err error) {
	return nil, errors.Errorf("cfssl transport not supported on windows")
}
