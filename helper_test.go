package main

import (
	"net/http"
	"path/filepath"
	"testing"

	"github.com/crewjam/saml/samlsp"
	spoe "github.com/criteo/haproxy-spoe-go"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func Test_contains(t *testing.T) {
	tests := []struct {
		a     []string
		b     string
		found bool
	}{
		{[]string{"item 1", "item 2"}, "item 3", false},
		{[]string{"item 1", "item 2", "item 3"}, "item 3", true},
	}

	for _, tt := range tests {
		assert.Equal(t, tt.found, contains(tt.a, tt.b))
	}
}

func Test_parseStringHeader(t *testing.T) {
	tests := []struct {
		name    string
		s       string
		h       http.Header
		wanterr bool
	}{
		{"no headers, no trailing blank", "", nil, true},
		{"no headers, trailing blank", "\r\n", http.Header{}, false},
		{"single header, no trailing blank", "Host: blah\n", nil, true},
		{"single header, trailing blank", "Host: blah\r\n\r\n", http.Header{"Host": {"blah"}}, false},
		{"two headers, no trailing blank", "Host: blah\nContent-Type: text/html\n", nil, true},
		{"two lowercase headers, no trailing blank", "host: blah\ncontent-type: text/html\n", nil, true},
		{"three lowercase headers, no trailing blank", "host: blah\r\ncontent-type: text/html\r\ncontent-type: text/json\r\n", nil, true},
		{"three lowercase headers, trailing blank", "host: blah\r\ncontent-type: text/html\r\ncontent-type: text/json\r\n\r\n", http.Header{
			"Host":         {"blah"},
			"Content-Type": {"text/html", "text/json"},
		}, false},
	}
	for _, tt := range tests {
		h, err := parseStringHeader(tt.s)
		if tt.wanterr {
			assert.Equal(t, tt.h, h, tt.name)
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Equal(t, tt.h, h, tt.name)
			assert.Nil(t, err, tt.name)
		}
	}
}

func Test_parseBinaryHeader(t *testing.T) {
	tests := []struct {
		name    string
		b       []byte
		h       http.Header
		wanterr bool
	}{
		{"invalid: empty headers", []byte{}, nil, true},
		{"valid: no headers, trailing blank", []byte{0, 0}, http.Header{}, false},
		{"invalid: single header, no trailing blank", []byte{
			4, 72, 111, 115, 116,
			4, 98, 108, 97, 104,
		}, nil, true},
		{"valid: single header, trailing blank", []byte{
			4, 72, 111, 115, 116,
			4, 98, 108, 97, 104,
			0, 0,
		}, http.Header{"Host": {"blah"}}, false},
		{"invalid: two headers, no trailing blank", []byte{
			4, 72, 111, 115, 116,
			4, 98, 108, 97, 104,
			12, 67, 111, 110, 116, 101, 110, 116, 45, 84, 121, 112, 101,
			9, 116, 101, 120, 116, 47, 104, 116, 109, 108,
		}, nil, true},
		{"valid: two headers, trailing blank", []byte{
			4, 72, 111, 115, 116,
			4, 98, 108, 97, 104,
			12, 67, 111, 110, 116, 101, 110, 116, 45, 84, 121, 112, 101,
			9, 116, 101, 120, 116, 47, 104, 116, 109, 108,
			0, 0,
		}, http.Header{"Host": {"blah"}, "Content-Type": {"text/html"}}, false},
		{"invalid: header name only", []byte{
			4, 72, 111, 115, 116,
		}, nil, true},
		{"invalid: header name wrong size", []byte{
			5, 72, 111, 115, 116,
		}, nil, true},
		{"invalid: invalid value length", []byte{
			4, 72, 111, 115, 116,
			10, 98, 108, 97, 104,
			0, 0,
		}, nil, true},
		{"valid: trailing garbage", []byte{
			4, 72, 111, 115, 116,
			4, 98, 108, 97, 104,
			0, 0,
			4, 72, 111, 115, 116,
		}, http.Header{"Host": {"blah"}}, false},
	}
	for _, tt := range tests {
		h, err := parseBinaryHeader(tt.b)
		if tt.wanterr {
			assert.Equal(t, tt.h, h, tt.name)
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Equal(t, tt.h, h, tt.name)
			assert.Nil(t, err, tt.name)
		}
	}
}

func Test_makeKeyAbsPath(t *testing.T) {
	// test setup
	viper.Reset()
	viper.Set("basepath", string(filepath.Separator))
	viper.Set("relative", "server.key")
	viper.Set("absolute", string(filepath.Separator)+"app"+string(filepath.Separator)+"server.key")

	tests := []struct {
		key     string
		want    string
		wanterr bool
	}{
		{"relative", string(filepath.Separator) + "server.key", false},
		{"absolute", string(filepath.Separator) + "app" + string(filepath.Separator) + "server.key", false},
		{"missing", "", true},
	}

	for _, tt := range tests {
		err := makeKeyAbsPath(tt.key)
		if tt.wanterr {
			assert.NotNil(t, err)
		} else {
			val := viper.GetString(tt.key)
			assert.Equal(t, tt.want, val)
		}
	}
}

func Test_makeKeyAbsPathAndGet(t *testing.T) {
	// test setup
	viper.Reset()
	viper.Set("basepath", string(filepath.Separator))
	viper.Set("relative", "server.key")
	viper.Set("absolute", string(filepath.Separator)+"app"+string(filepath.Separator)+"server.key")

	tests := []struct {
		key     string
		want    string
		wanterr bool
	}{
		{"relative", string(filepath.Separator) + "server.key", false},
		{"absolute", string(filepath.Separator) + "app" + string(filepath.Separator) + "server.key", false},
		{"missing", "", true},
	}

	for _, tt := range tests {
		got, err := makeKeyAbsPathAndGet(tt.key)
		if tt.wanterr {
			assert.NotNil(t, err)
		} else {
			assert.Equal(t, tt.want, got)
		}
	}
}

func Test_parseClaims(t *testing.T) {
	tests := []struct {
		name   string
		claims map[string]string
		want   []spoe.Action
	}{
		{
			"no claims",
			map[string]string{},
			[]spoe.Action{},
		},
		{
			"one claim",
			map[string]string{
				"name": "bob",
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "name",
					Scope: spoe.VarScopeTransaction,
					Value: "bob",
				},
			},
		},
		{
			"two claims",
			map[string]string{
				"name":  "bob",
				"email": "bob@bob.net",
			},
			[]spoe.Action{
				spoe.ActionSetVar{
					Name:  "name",
					Scope: spoe.VarScopeTransaction,
					Value: "bob",
				},
				spoe.ActionSetVar{
					Name:  "email",
					Scope: spoe.VarScopeTransaction,
					Value: "bob@bob.net",
				},
			},
		},
	}

	for _, tt := range tests {
		got := parseClaims(tt.claims)
		assert.Equal(t, tt.want, got, tt.name)
	}
}

func Test_constraintsValid(t *testing.T) {
	tests := []struct {
		name        string
		constraints map[string][]string
		claims      map[string]string
		want        bool
	}{
		{
			"no constraints or claims",
			nil,
			nil,
			true,
		},
		{
			"no constraints or claims again",
			map[string][]string{},
			nil,
			true,
		},
		{
			"constrains with no claims",
			map[string][]string{
				"name": {"bob"},
			},
			nil,
			false,
		},
		{
			"constrains with claims (no match)",
			map[string][]string{
				"name": {"bob"},
			},
			map[string]string{
				"name": "fred",
			},
			false,
		},
		{
			"constrains with claims (match)",
			map[string][]string{
				"name": {"bob"},
			},
			map[string]string{
				"name": "bob",
			},
			true,
		},
		{
			"multiple constrains with claims (match)",
			map[string][]string{
				"name": {"fred", "bob"},
			},
			map[string]string{
				"name": "bob",
			},
			true,
		},
		{
			"multiple constrains with mismatched claims (no match)",
			map[string][]string{
				"name": {"fred", "bob"},
			},
			map[string]string{
				"email": "bob@bob.net",
			},
			false,
		},
	}

	for _, tt := range tests {
		got := constraintsValid(tt.constraints, tt.claims)
		assert.Equal(t, tt.want, got, tt.name)
	}
}

func Test_parseToken(t *testing.T) {
	tests := []struct {
		name       string
		attributes samlsp.Attributes
		mapping    map[string]string
		want       map[string]string
	}{
		{"all nill", nil, nil, map[string]string{}},
		{
			"no mapping - one attribute",
			samlsp.Attributes{
				"name": []string{"bob"},
			},
			nil,
			map[string]string{
				"name": "bob",
			},
		},
		{
			"no mapping - one multi-value attribute",
			samlsp.Attributes{
				"name": []string{"bob", "bloggs"},
			},
			nil,
			map[string]string{
				"name": "bob",
			},
		},
		{
			"no mapping - multiple attributes",
			samlsp.Attributes{
				"name":  []string{"bob"},
				"email": []string{"bob@bob.net"},
			},
			nil,
			map[string]string{
				"name":  "bob",
				"email": "bob@bob.net",
			},
		},
		{
			"map only mail to email - multiple attributes",
			samlsp.Attributes{
				"name": []string{"bob"},
				"mail": []string{"bob@bob.net"},
			},
			map[string]string{
				"email": "mail",
			},
			map[string]string{
				"email": "bob@bob.net",
			},
		},
		{
			"map all - multiple attributes",
			samlsp.Attributes{
				"name": []string{"bob"},
				"mail": []string{"bob@bob.net"},
			},
			map[string]string{
				"name":  "name",
				"email": "mail",
			},
			map[string]string{
				"name":  "bob",
				"email": "bob@bob.net",
			},
		},
		{
			"mapping mismatch - multiple attributes",
			samlsp.Attributes{
				"name": []string{"bob"},
				"mail": []string{"bob@bob.net"},
			},
			map[string]string{
				"phone":  "ph",
				"mobile": "mob",
			},
			map[string]string{},
		},
	}

	for _, tt := range tests {
		got := parseToken(tt.attributes, tt.mapping)
		assert.Equal(t, tt.want, got, tt.name)
	}

}
