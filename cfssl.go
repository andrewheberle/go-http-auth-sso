// +build !windows

package main

import (
	"fmt"
	"time"

	"github.com/cloudflare/cfssl/csr"
	"github.com/cloudflare/cfssl/transport"
	"github.com/cloudflare/cfssl/transport/core"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func startCFSSLListener() (l *transport.Listener, err error) {

	cfsslLogger := log.WithField("source", "cfssl")
	cfsslLogger.WithField("cn", viper.GetString("tls.cfssl.cn")).Debug("creating core.Identity")
	cfsslLogger.WithField("private_key", viper.GetString("tls.key")).Debug("creating core.Identity")
	cfsslLogger.WithField("certificate", viper.GetString("tls.certificate")).Debug("creating core.Identity")
	cfsslLogger.WithField("profile", viper.GetString("tls.cfssl.profile")).Debug("creating core.Identity")
	cfsslLogger.WithField("remote", viper.GetString("tls.cfssl.remote")).Debug("creating core.Identity")

	// ensure Hosts contains CN
	cfsslHosts := viper.GetStringSlice("tls.cfssl.hosts")
	if !contains(cfsslHosts, viper.GetString("tls.cfssl.cn")) {
		cfsslHosts = append(cfsslHosts, viper.GetString("tls.cfssl.cn"))
	}
	// create new cfssl identity
	id := &core.Identity{
		Request: &csr.CertificateRequest{
			CN:    viper.GetString("tls.cfssl.cn"),
			Hosts: cfsslHosts,
		},
		Profiles: map[string]map[string]string{
			"paths": {
				"private_key": viper.GetString("tls.key"),
				"certificate": viper.GetString("tls.certificate"),
			},
			"cfssl": {
				"profile": viper.GetString("tls.cfssl.profile"),
				"remote":  viper.GetString("tls.cfssl.remote"),
			},
		},
	}
	if viper.IsSet("tls.cfssl.auth_type") && viper.IsSet("tls.cfssl.auth_key") {
		cfsslLogger.WithField("auth_type", viper.GetString("tls.cfssl.auth_type")).Debug("creating core.Identity")
		cfsslLogger.WithField("auth_key", viper.GetString("tls.cfssl.auth_key")).Debug("creating core.Identity")
		id.Profiles["cfssl"]["auth-type"] = viper.GetString("tls.cfssl.auth_type")
		id.Profiles["cfssl"]["auth-key"] = viper.GetString("tls.cfssl.auth_key")
	}

	// Renew 72 hours before expiry
	tr, err := transport.New(72*time.Hour, id)
	if err != nil {
		return nil, err
	}

	l, err = transport.Listen(fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("port")), tr)
	if err != nil {
		return nil, err
	}

	return l, nil
}
