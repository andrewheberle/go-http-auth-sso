package main

import (
	errors "golang.org/x/xerrors"
)

func decodeVarint(b []byte) (int, int, error) {
	if len(b) == 0 {
		return 0, 0, errors.Errorf("decode varint: empty slice")
	}

	val := int(b[0])
	off := 1

	if val < 240 {
		return val, 1, nil
	}

	r := uint(4)
	for {
		if off > len(b)-1 {
			return 0, 0, errors.Errorf("decode varint: unterminated sequence")
		}

		v := int(b[off])
		val += v << r
		off++
		r += 7

		if v < 128 {
			break
		}
	}

	return val, off, nil
}

func decodeBytes(b []byte) ([]byte, int, error) {
	l, off, err := decodeVarint(b)
	if err != nil {
		return nil, 0, errors.Errorf("decode bytes: %w", err)
	}

	if len(b) < l+off {
		return nil, 0, errors.Errorf("decode bytes: unterminated sequence")
	}

	return b[off : off+l], off + l, nil
}

func decodeString(b []byte) (string, int, error) {
	b, n, err := decodeBytes(b)
	return string(b), n, err
}
