package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_startCFSSLListener(t *testing.T) {
	tests := []struct {
		name    string
		wantL   *transportListener
		wantErr bool
	}{
		{"should always error", &transportListener{}, true},
	}
	for _, tt := range tests {
		_, err := startCFSSLListener()
		if tt.wantErr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
		}
	}
}
