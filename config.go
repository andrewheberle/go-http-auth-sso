package main

import (
	"net"
	"path/filepath"
	"strings"

	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"net/url"

	"github.com/crewjam/saml/samlsp"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	errors "golang.org/x/xerrors"
)

type serviceProviders map[string]serviceProvider

type serviceProvider struct {
	Host         string
	ClaimMapping map[string]string
	LoginURL     string
	Provider     *samlsp.Middleware
}

// constants
const (
	DefaultLoginURL   = "/auth/login"
	DefaultCookieName = "token"
)

func initViper(configFile string) {
	viper.SetDefault("basepath", ".")
	viper.SetDefault("address", "127.0.0.1")
	viper.SetDefault("port", 8443)
	viper.SetDefault("spoe_port", 9000)
	viper.SetDefault("healthcheck_url", "/healthz")
	viper.SetDefault("log_healthchecks", false)
	viper.SetDefault("tls.key", "server.key")
	viper.SetDefault("tls.certificate", "server.pem")
	viper.SetDefault("tls.method", "user")
	viper.SetDefault("tls.cfssl", map[string]string{
		"cn":          "localhost",
		"profile":     "server",
		"remote":      "localhost:8000",
		"auth_method": "standard",
	})

	// read in config
	viper.SetConfigName(strings.TrimSuffix(filepath.Base(configFile), filepath.Ext(configFile)))
	viper.AddConfigPath(filepath.Dir(configFile))
}

func newServiceProviderList() (splist serviceProviders, err error) {

	var claimMapping map[string]string

	if !viper.IsSet("auth") {
		return nil, errors.Errorf("auth section missing")
	}

	splist = make(serviceProviders)
	auth := viper.Sub("auth")
	if auth == nil {
		return nil, errors.Errorf("auth section was empty")
	}

	// basepath := viper.GetString("basepath")
	configLogger := log.WithFields(log.Fields{"source": "config", "action": "create-sp"})

	for sp := range auth.AllSettings() {
		spLogger := configLogger.WithField("sp", sp)

		spLogger.Debug("creating service provider")

		// validate root
		if !auth.IsSet(sp + ".root") {
			return nil, errors.Errorf("%s: root missing", sp)
		}

		rootURL, err := url.Parse(auth.GetString(sp + ".root"))
		if err != nil {
			return nil, err
		}

		spLogger.WithField("root", rootURL).Debug("parsed root")

		// start building samlsp Options
		spOptions := samlsp.Options{
			URL: *rootURL,
		}

		// validate key
		spKey, err := makeSubKeyAbsPathAndGet(auth, sp+".key")
		if err != nil {
			return nil, errors.Errorf("%s: %w", sp, err)
		}

		spLogger.WithField("key", spKey).Debug("parsed key")

		// validate cert
		spCert, err := makeSubKeyAbsPathAndGet(auth, sp+".certificate")
		if err != nil {
			return nil, errors.Errorf("%s: %w", sp, err)
		}

		spLogger.WithField("certificate", spCert).Debug("parsed certificate")

		// build keypair
		keyPair, err := tls.LoadX509KeyPair(spCert, spKey)
		if err != nil {
			return nil, errors.Errorf("LoadX509KeyPair: %w", err)
		}

		keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
		if err != nil {
			return nil, errors.Errorf("ParseCertificate: %w", err)
		}

		// add to samlsp Options
		spOptions.Key = keyPair.PrivateKey.(*rsa.PrivateKey)
		spOptions.Certificate = keyPair.Leaf

		// validate metadata
		if !auth.IsSet(sp + ".metadata") {
			return nil, errors.Errorf("%s: metadata missing", sp)
		}

		mURL, err := url.Parse(auth.GetString(sp + ".metadata"))
		if err != nil {
			return nil, err
		}

		// add to samlsp Options
		spOptions.IDPMetadataURL = mURL

		// handle options that can be defaulted
		auth.SetDefault(sp+".cookie_name", DefaultCookieName)
		auth.SetDefault(sp+".cookie_domain", rootURL.Hostname())
		auth.SetDefault(sp+".login_url", DefaultLoginURL)

		// cookie_name
		spOptions.CookieName = auth.GetString(sp + ".cookie_name")

		// cookie_domain
		spOptions.CookieDomain = auth.GetString(sp + ".cookie_domain")

		samlSP, err := samlsp.New(spOptions)
		if err != nil {
			return nil, err
		}

		if auth.IsSet(sp + ".claim_mapping") {
			claimMapping = auth.GetStringMapString(sp + ".claim_mapping")
		}

		splist[sp] = serviceProvider{
			Host:         rootURL.Hostname(),
			ClaimMapping: claimMapping,
			Provider:     samlSP,
			LoginURL:     auth.GetString(sp + ".login_url"),
		}
	}

	if viper.GetString("tls.method") == "cfssl" {
		cfsslHosts := viper.GetStringSlice("tls.cfssl.hosts")
		// Ensure Hosts containst CN
		if !contains(cfsslHosts, viper.GetString("tls.cfssl.cn")) {
			cfsslHosts = append(cfsslHosts, viper.GetString("tls.cfssl.cn"))
		}

		// Check that the saml SP names are added to Hosts
		for k := range splist {
			if !contains(cfsslHosts, k) {
				cfsslHosts = append(cfsslHosts, k)
			}
		}
		viper.Set("tls.cfssl.hosts", cfsslHosts)
	}

	return splist, nil
}

func validateConfig() error {
	// Clean data
	basepath, err := filepath.Abs(viper.GetString("basepath"))
	if err != nil {
		return err
	}
	// Write back to viper
	viper.Set("basepath", basepath)

	// Make certain values absolute as required
	keys := []string{"tls.certificate", "tls.key"}
	for _, k := range keys {
		if err := makeKeyAbsPath(k); err != nil {
			return err
		}
	}

	listenIP := net.ParseIP(viper.GetString("address"))
	if listenIP == nil {
		return errors.Errorf("invalid ip address : %w", viper.GetString("address"))
	}

	// ensure tls.method is valid
	if !contains([]string{"user", "cfssl"}, viper.GetString("tls.method")) {
		return errors.Errorf("tls.method is invalid")
	}

	// ensure cfssl part of config is present
	if viper.GetString("tls.method") == "cfssl" && !viper.IsSet("tls.cfssl") {
		return errors.Errorf("cfssl method requested but cfssl section missing")
	}

	// ensure auth section is found
	if !viper.IsSet("auth") {
		return errors.Errorf("auth section missing")
	}

	return nil
}
