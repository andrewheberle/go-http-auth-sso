package main

import (
	"path/filepath"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func Test_validateConfig(t *testing.T) {
	tests := []struct {
		name            string
		config          string
		wantInitErr     bool
		wantValidateErr bool
	}{
		{"test missing config", "config-missing.yml", true, false},
		{"test empty config", "config-empty.yml", false, true},
		{"test user config", "config-user.yml", false, false},
		{"test cfssl config", "config-cfssl.yml", false, false},
		{"test invalid address config", "config-invalid-address.yml", false, true},
		{"test invalid tls method config", "config-invalid-tlsmethod.yml", false, true},
	}

	for _, tt := range tests {
		viper.Reset()
		initViper(filepath.Join("testdata", tt.config))
		if tt.wantInitErr {
			assert.NotNil(t, viper.ReadInConfig(), tt.name)
			break
		} else {
			if !assert.Nil(t, viper.ReadInConfig(), tt.name) {
				return
			}
		}
		if tt.wantValidateErr {
			assert.NotNil(t, validateConfig(), tt.name)
		} else {
			assert.Nil(t, validateConfig(), tt.name)
		}
	}
}
