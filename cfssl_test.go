// +build !windows

package main

import (
	"testing"

	"github.com/cloudflare/cfssl/transport"
	"github.com/stretchr/testify/assert"
)

func Test_startCFSSLListener(t *testing.T) {
	tests := []struct {
		name    string
		wantL   *transport.Listener
		wantErr bool
	}{
		{"should always error", &transport.Listener{}, true},
	}
	for _, tt := range tests {
		_, err := startCFSSLListener()
		if tt.wantErr {
			assert.NotNil(t, err, tt.name)
		} else {
			assert.Nil(t, err, tt.name)
		}
	}
}
